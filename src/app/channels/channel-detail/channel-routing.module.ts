import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from '../not-found.component';

const channelRoutes: Routes = [
    { path: 'channel/:id/overview', component: PageNotFoundComponent },
    { path: 'channel/:id/commands' },
    { path: 'channel/:id/quotes' },
    { path: 'channel/:id/variables' },
    { path: 'channel/:id/auto-replies' },
    { path: 'channel/:id/scheduled-commands' },
    { path: 'channel/:id/regulars' },
    { path: 'channel/:id/chat-rules' },
    { path: 'channel/:id/past-broadcasts' }
]

@NgModule({
    declarations: [
  ],
  imports: [
    RouterModule.forRoot(channelRoutes)
  ]
})
export class ChannelRoutingModule { }
export class Channel {
    channelID : string;
    channelName : string;

    enabled: boolean;
    commandPrefix: string;
    bullet: string;
    cooldown: number;
    mode: number;
    
    lastfm: string;
    extraLifeID: string;
    timeoutDuration: number;
    commercialLength: number;

    signKicks: boolean;
    enableWarnings: boolean;
    useFilters: boolean;

    steamID: string;
    shouldModerate: boolean;
    subscriberAlert: boolean;
    subscriberRegulars: boolean;
    subMessage: string;

    clickToTweetFormat: string;
    parseYoutube: boolean;
    urbanEnabled: boolean;

    rollTimeout: boolean;
    rollLevel: string;
    rollCooldown: number;
    rollDefault: number;
}
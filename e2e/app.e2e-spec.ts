import { Coebotv2HttpPage } from './app.po';

describe('coebotv2-http App', () => {
  let page: Coebotv2HttpPage;

  beforeEach(() => {
    page = new Coebotv2HttpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
